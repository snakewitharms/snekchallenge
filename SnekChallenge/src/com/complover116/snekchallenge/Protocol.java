package com.complover116.snekchallenge;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Protocol {
    static final byte TYPE_TICK = 1;
    static final byte TYPE_KEYPRESS = 2;
    static void sendTickInfo(ArrayList<Player> players, Fruit fruit, Socket connection) throws IOException {
        ByteBuffer data = ByteBuffer.allocate(64);
        data.put(TYPE_TICK);
        data.putInt(players.size());
        for (Player player : players) {
            data.putInt(player.snake.segments.get(0).x);
            data.putInt(player.snake.segments.get(0).y);
            data.putInt(player.snake.gainedSegment ? 1 : 0);
        }
        data.putInt(fruit.x);
        data.putInt(fruit.y);
        connection.getOutputStream().write(data.array());
    }

    static void getTickInfo(Socket connection, SnekPanel snekPanel) throws IOException {
        byte[] b = new byte[64];
        connection.getInputStream().read(b);
        ByteBuffer data = ByteBuffer.wrap(b);
        data.get(); // TYPE_TICK
        int player_count = data.getInt(); // Player count (2)

        synchronized (snekPanel.snakes) {
            for (int i = 0; i < player_count; i++) {
                int x = data.getInt();
                int y = data.getInt();
                if (data.getInt() == 1) {
                    Snake.Segment lastSegment = snekPanel.snakes.get(i).segments.get(snekPanel.snakes.get(i).segments.size()-1);
                    snekPanel.snakes.get(i).segments.add(new Snake.Segment(lastSegment.x, lastSegment.y));
                    snekPanel.snakes.get(i).segments.add(new Snake.Segment(lastSegment.x, lastSegment.y));
                }
                snekPanel.snakes.get(i).updateSegments(x, y);
            }
            snekPanel.fruit.x = data.getInt();
            snekPanel.fruit.y = data.getInt();
        }
        snekPanel.repaint();
    }

    static void sendKeyPress(Socket connection, int keyCode) {
        ByteBuffer data = ByteBuffer.allocate(64);
        data.put(TYPE_KEYPRESS);
        data.putInt(keyCode);
        try {
            connection.getOutputStream().write(data.array());
            System.out.println("Key sent "+keyCode);
        } catch (IOException e) {

        }
    }

    static int getKeyPress(Socket connection) throws IOException {
        byte[] b = new byte[64];
        connection.getInputStream().read(b);
        ByteBuffer data = ByteBuffer.wrap(b);
        data.get(); // TYPE_KEYPRESS
        int key = data.getInt();
        System.out.println("Key received "+key);
        return key;
    }
}
