package com.complover116.snekchallenge;

import javax.swing.*;

public class Main {
    static final int PLAYER_COUNT = 2;
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame("Snek");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SnekPanel snekPanel = new SnekPanel();
        mainFrame.add(snekPanel);
        snekPanel.setFocusable(true);


        mainFrame.pack();
        mainFrame.setSize(508, 578);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
        snekPanel.requestFocusInWindow();

        String connectionAddr = JOptionPane.showInputDialog(mainFrame, "Type server address or leave blank to host");

        if (connectionAddr.length() == 0) {
            Server server = new Server();
            new Thread(server, "Server Thread").start();
            Client client = new Client("localhost", snekPanel);
            new Thread(client, "Client Thread").start();
        } else {
            Client client = new Client(connectionAddr, snekPanel);
            new Thread(client, "Client Thread").start();
        }

    }
}
