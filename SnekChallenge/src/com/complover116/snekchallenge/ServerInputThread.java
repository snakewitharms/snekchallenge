package com.complover116.snekchallenge;

import java.awt.event.KeyEvent;
import java.io.IOException;

public class ServerInputThread implements Runnable {

    Player player;
    ServerInputThread(Player player) {
        this.player = player;
    }
    @Override
    public void run() {
        try {
            while (true) {
                int key = Protocol.getKeyPress(player.connection);
                synchronized (player.snake) {
                    switch (key) {
                        case KeyEvent.VK_RIGHT:
                            if (player.snake.lastDirection != Snake.Direction.LEFT)
                                player.snake.direction = Snake.Direction.RIGHT;
                            break;
                        case KeyEvent.VK_LEFT:
                            if (player.snake.lastDirection != Snake.Direction.RIGHT)
                                player.snake.direction = Snake.Direction.LEFT;
                            break;
                        case KeyEvent.VK_DOWN:
                            if (player.snake.lastDirection != Snake.Direction.UP)
                                player.snake.direction = Snake.Direction.DOWN;
                            break;
                        case KeyEvent.VK_UP:
                            if (player.snake.lastDirection != Snake.Direction.DOWN)
                                player.snake.direction = Snake.Direction.UP;
                            break;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("ServerInputThread exited");
        }
    }
}
